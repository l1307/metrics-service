package com.leboncoin.metrics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application initialization class.
 * </p>
 * - All @ {@ link Component} of sub-packages of this class are scanned <br>
 * - Spring configuration files (application.properties | yml | yaml) <br>
 * are loaded <br>
 * - @ {@ link PropertySource} annotation .properties files are loaded <br>
 * - The auto-configuration of the spring boot is triggered
 *
 */
@SpringBootApplication
public class MetricsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MetricsApplication.class, args);
	}

}
