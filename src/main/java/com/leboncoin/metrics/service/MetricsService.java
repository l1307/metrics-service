package com.leboncoin.metrics.service;

import com.generated.fizzbuzz.FizzBuzzSchema;
import com.leboncoin.metrics.model.FizzBuzzMetricDTO;

/**
 * MetricsService interface.
 *
 * This service increment metric in database and fetch the highest metrics
 */
public interface MetricsService {

    /**
     * Create or update a metric
     *
     * @param fizzBuzzEntryDTO the {@link FizzBuzzSchema} to create or update
     */
    public void incrementMetric(FizzBuzzSchema fizzBuzzEntryDTO);
    
    /**
     * Fetch the highest metrics
     *
     * @return the {@link FizzBuzzMetricDTO} from database.
     */
    public FizzBuzzMetricDTO getMetrics();
    
}
