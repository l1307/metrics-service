package com.leboncoin.metrics.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import com.generated.fizzbuzz.FizzBuzzSchema;
import com.leboncoin.metrics.model.FizzBuzzMetricDTO;
import com.leboncoin.metrics.provider.entity.FizzBuzzMetricEntity;
import com.leboncoin.metrics.provider.repository.FizzBuzzRepository;
import com.leboncoin.metrics.service.MetricsService;

/**
 * Implementation of the {@link MetricsService} interface.
 *
 * This service increment metric in database and fetch the highest metrics
 */
@Service
public class MetricsServiceImpl implements MetricsService {

    /** Database repository injected by Spring */
    @Autowired
    FizzBuzzRepository fizzBuzzRepository;
    
    /**
     * Create or update a metric
     *
     * @param fizzBuzzEntryDTO the {@link FizzBuzzSchema} to create or update
     */
    @Override
    public void incrementMetric(final FizzBuzzSchema fizzBuzzEntryDTO) {

        final FizzBuzzMetricEntity metric = fizzBuzzRepository.findByLimitAndInt1AndInt2AndStr1AndStr2(
                        fizzBuzzEntryDTO.getLimit(), fizzBuzzEntryDTO.getInt1(), fizzBuzzEntryDTO.getInt2(), fizzBuzzEntryDTO.getStr1(), fizzBuzzEntryDTO.getStr2());
        
        if(metric == null) {
            fizzBuzzRepository.save(new FizzBuzzMetricEntity(fizzBuzzEntryDTO));
        } else {
            metric.setCount(metric.getCount() + 1);
            fizzBuzzRepository.save(metric);
        }
        
    }

    /**
     * Fetch the highest metrics
     *
     * @return the {@link FizzBuzzMetricDTO} from database.
     */
    @Override
    public FizzBuzzMetricDTO getMetrics() {
        
        FizzBuzzMetricEntity metricEntity = fizzBuzzRepository.findTopByOrderByCountDesc();
        
        if(metricEntity == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No metrics found");   
        }        
        
        return new FizzBuzzMetricDTO(metricEntity);
    }

}
