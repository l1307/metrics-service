package com.leboncoin.metrics.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.leboncoin.metrics.model.FizzBuzzMetricDTO;
import com.leboncoin.metrics.service.MetricsService;

/**
 * REST metrics management controller.
 * <p>
 * - we avoid triggering validations in the controller (unless they are specific to the REST layer), but we trigger them instead
 * in the service layer in order to be shared regardless of the controller. <br>
 * - for the methods for which we want to be able to return headers (Location for example), or return different responses in
 * depending on the feedback from the service layer, we use a return type {@link ResponseEntity} allowing to control more finely the
 * HTTP response. <br>
 * - to return errors / problems to the customer containing a body with the details of the problem, we will privilege exceptional reports
 * of the type {@link ResponseStatusException} with status code for errors
 *
 */
@RestController
@RequestMapping(path = "/")
public class MetricsRestController {

    /** Business service injected by Spring */
    @Autowired
    MetricsService metricsService;
    
    /**
     * REST GET request to fetch a Resource {@link FizzBuzzMetricDTO}.
     *
     * @return the fetched resource is wrapped in a {@link FizzBuzzMetricDTO} resource and a 200 code
     */
    @GetMapping
    public ResponseEntity<FizzBuzzMetricDTO> getMetrics() {
        return ResponseEntity.ok(metricsService.getMetrics());
    }

}
