package com.leboncoin.metrics.controller.listener;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import com.generated.fizzbuzz.FizzBuzzSchema;
import com.leboncoin.metrics.service.MetricsService;

/**
 * KAFKA metrics management listener.
 * Configuration of this listener is managed into application.yml
 */
@Component
public class EvenementFizzBuzzListener {

    /** Logger */
    private static final Logger LOGGER = LogManager.getLogger();
    
    /** Business service injected by Spring */
    @Autowired
    MetricsService metricsService;
    
    /**
     * Kafka <b>Transactional</b> listener of {@link FizzBuzzSchema} type records. The Kafka transaction is started
     * automatically by Spring Kafka. The service called from this class will start a no data transaction using the
     * {@link DataSourceTransactionManager} associated.
     * Please note that we do not necessarily receive all the records of the topic: only the commited records (or not part of a
     * transaction) => cf. config spring-kafka, and only the records corresponding to the filtering in place
     */
    @KafkaListener(topics = "${app.topic}")
    public void listen(@Payload ConsumerRecord<String, FizzBuzzSchema> record) {

        LOGGER.info("FizzBuzzSchema receive from queue with following data 'limit: {}', 'int1: {}', 'int2: {}', 'str1: {}', 'str2: {}'", 
                        record.value().getLimit(),
                        record.value().getInt1(),
                        record.value().getInt2(),
                        record.value().getStr1(),
                        record.value().getStr2());
        metricsService.incrementMetric(record.value());
        
    }


}
