package com.leboncoin.metrics.provider.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.leboncoin.metrics.provider.entity.FizzBuzzMetricEntity;

/**
 * FizzBuzzRepository interface.
 *
 * This provider save and retreive {@link FizzBuzzMetricEntity} from database
 */
@Repository
public interface FizzBuzzRepository extends JpaRepository<FizzBuzzMetricEntity, Long> {

    FizzBuzzMetricEntity findByLimitAndInt1AndInt2AndStr1AndStr2(Integer limit, Integer int1, Integer int2, String str1, String str2);
    
    FizzBuzzMetricEntity findTopByOrderByCountDesc();
}