package com.leboncoin.metrics.provider.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.generated.fizzbuzz.FizzBuzzSchema;

/**
 * FizzBuzzMetricEntity is an objet which represents an entity from fizz_buzz_metrics table.
 *
 * All his attributes represent a field from each column of fizz_buzz_metrics table
 */
@Entity
@Table(name = "fizz_buzz_metrics")
public class FizzBuzzMetricEntity {
    
    public FizzBuzzMetricEntity() {};
    
    public FizzBuzzMetricEntity(FizzBuzzSchema fizzBuzzEntryDTO) {
        this.limit = fizzBuzzEntryDTO.getLimit();
        this.int1 = fizzBuzzEntryDTO.getInt1();
        this.int2 = fizzBuzzEntryDTO.getInt2();
        this.str1 = fizzBuzzEntryDTO.getStr1();
        this.str2 = fizzBuzzEntryDTO.getStr2();
        this.count = 1;
    };

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fizz_buzz_metrics_generator")
    @SequenceGenerator(name="fizz_buzz_metrics_generator", sequenceName = "fizz_buzz_metrics_seq", allocationSize=1)
    @Column(name = "fizz_buzz_metrics_id")
    private long id;
    
    @Column(name = "fizz_buzz_limit")
    private Integer limit;
    
    @Column(name = "fizz_buzz_int_1")
    private Integer int1;
    
    @Column(name = "fizz_buzz_int_2")
    private Integer int2;
    
    @Column(name = "fizz_buzz_str_1")
    private String str1;
    
    @Column(name = "fizz_buzz_str_2")
    private String str2;
    
    @Column(name = "fizz_buzz_count")
    private Integer count;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getInt1() {
        return int1;
    }

    public void setInt1(Integer int1) {
        this.int1 = int1;
    }

    public Integer getInt2() {
        return int2;
    }

    public void setInt2(Integer int2) {
        this.int2 = int2;
    }

    public String getStr1() {
        return str1;
    }

    public void setStr1(String str1) {
        this.str1 = str1;
    }

    public String getStr2() {
        return str2;
    }

    public void setStr2(String str2) {
        this.str2 = str2;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
    
}