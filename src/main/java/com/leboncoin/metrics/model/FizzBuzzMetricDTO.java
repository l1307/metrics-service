package com.leboncoin.metrics.model;

import com.generated.fizzbuzz.FizzBuzzSchema;
import com.leboncoin.metrics.provider.entity.FizzBuzzMetricEntity;

/**
 * Application output
 *
 */
public class FizzBuzzMetricDTO extends FizzBuzzSchema {
    
    public FizzBuzzMetricDTO(FizzBuzzMetricEntity fizzBuzzMetricEntity) {
        this.setLimit(fizzBuzzMetricEntity.getLimit());
        this.setInt1(fizzBuzzMetricEntity.getInt1());
        this.setInt2(fizzBuzzMetricEntity.getInt2());
        this.setStr1(fizzBuzzMetricEntity.getStr1());
        this.setStr2(fizzBuzzMetricEntity.getStr2());
        this.setOccurencesQuantity(fizzBuzzMetricEntity.getCount());
    }
    
    private Integer occurencesQuantity;

    public Integer getOccurencesQuantity() {
        return occurencesQuantity;
    }

    public void setOccurencesQuantity(Integer occurencesQuantity) {
        this.occurencesQuantity = occurencesQuantity;
    }

}
