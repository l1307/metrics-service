package com.leboncoin.metrics.controller.rest;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.server.ResponseStatusException;
import com.generated.fizzbuzz.FizzBuzzSchema;
import com.leboncoin.metrics.model.FizzBuzzMetricDTO;
import com.leboncoin.metrics.provider.entity.FizzBuzzMetricEntity;
import com.leboncoin.metrics.provider.repository.FizzBuzzRepository;
import com.leboncoin.metrics.service.MetricsService;
import com.leboncoin.metrics.service.impl.MetricsServiceImpl;
import com.leboncoin.metrics.spy.FizzBuzzRepositorySpy;

/**
 * This class test {@link MetricsRestController} and all relative services and provider
 * 
 * To ensure that application respect business rules, all test are based on rules defined on business rules inside doc folder
 *
 */
public class MetricsRestControllerTest {

    private MetricsRestController metricsRestController;
    
    private MetricsService metricsService;
    
    private FizzBuzzRepository fizzBuzzRepository;

    @BeforeEach
    public void setUp() {
        metricsRestController = new MetricsRestController();
        metricsService = new MetricsServiceImpl();
        fizzBuzzRepository = new FizzBuzzRepositorySpy();

        ReflectionTestUtils.setField(metricsService, "fizzBuzzRepository", fizzBuzzRepository);
        ReflectionTestUtils.setField(metricsRestController, "metricsService", metricsService);
    }

    /**
     * RULE_API_METRICS: Save two metrics entities and expect get the entity with the highest number of count
     */
    @Test
    public void save_metrics_and_check_that_fetching_highest() {

        // Prepare
        final FizzBuzzSchema fizzBuzzEntryDTO = createFizzBuzzSchema(1, 1, 2, "fizz", "buzz");
        final FizzBuzzMetricEntity higherEntity = new FizzBuzzMetricEntity(fizzBuzzEntryDTO);
        higherEntity.setCount(10);
        final FizzBuzzMetricEntity lowerEntity = new FizzBuzzMetricEntity(fizzBuzzEntryDTO);
        lowerEntity.setCount(9);
        
        fizzBuzzRepository.save(higherEntity);
        fizzBuzzRepository.save(lowerEntity);

        // Act
        ResponseEntity<FizzBuzzMetricDTO> result = metricsRestController.getMetrics();

        // Verify
        Assertions.assertThat(result.getBody())
        .usingRecursiveComparison()
        .ignoringFields("occurencesQuantity")
        .isEqualTo(fizzBuzzEntryDTO);
    }
    
    /**
     * RULE_API_METRICS_NOT_FOUND: Fetching the application when their is not metrics and expected an error 404
     */
    @Test
    public void fetching_empty_metrics_and_expect_not_found() {
        
        // Act
        Assertions.assertThatThrownBy(() -> {
            metricsRestController.getMetrics();
        })
        // Verify
        .isInstanceOf(ResponseStatusException.class)
                        .extracting(e -> ((ResponseStatusException) e))
                        .satisfies(e -> HttpStatus.NOT_FOUND.equals(e.getStatus()))
                        .satisfies(e -> "No metrics found".equals(e.getReason()));
    }
    
    private FizzBuzzSchema createFizzBuzzSchema(Integer limit, Integer int1, Integer int2, String str1, String str2) {
        final FizzBuzzSchema fizzBuzzSchema = new FizzBuzzSchema();
        fizzBuzzSchema.setLimit(limit);
        fizzBuzzSchema.setInt1(int1);
        fizzBuzzSchema.setInt2(int2);
        fizzBuzzSchema.setStr1(str1);
        fizzBuzzSchema.setStr2(str2);
        return fizzBuzzSchema;
    }

}
