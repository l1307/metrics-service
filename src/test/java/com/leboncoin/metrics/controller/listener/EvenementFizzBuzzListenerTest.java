package com.leboncoin.metrics.controller.listener;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;
import com.generated.fizzbuzz.FizzBuzzSchema;
import com.leboncoin.metrics.provider.repository.FizzBuzzRepository;
import com.leboncoin.metrics.service.MetricsService;
import com.leboncoin.metrics.service.impl.MetricsServiceImpl;
import com.leboncoin.metrics.spy.FizzBuzzRepositorySpy;

/**
 * This class test {@link EvenementFizzBuzzListener} and all relative services and provider
 * 
 * To ensure that application respect business rules, all test are based on rules defined on business rules inside doc folder
 *
 */
public class EvenementFizzBuzzListenerTest {

    private EvenementFizzBuzzListener evenementFizzBuzzListener;
    
    private MetricsService metricsService;
    
    private FizzBuzzRepository fizzBuzzRepository;

    @BeforeEach
    public void setUp() {
        evenementFizzBuzzListener = new EvenementFizzBuzzListener();
        metricsService = new MetricsServiceImpl();
        fizzBuzzRepository = new FizzBuzzRepositorySpy();

        ReflectionTestUtils.setField(metricsService, "fizzBuzzRepository", fizzBuzzRepository);
        ReflectionTestUtils.setField(evenementFizzBuzzListener, "metricsService", metricsService);
    }

    /**
     * RULE_INITIAL_STORE_EVENT: Listen a record, then verify that an entity was created on database
     */
    @Test
    public void listen_record_and_check_that_entity_is_stored() {

        // Prepare
        ConsumerRecord<String, FizzBuzzSchema> record =
                        new ConsumerRecord<>("topic", 0, 0, "key", createFizzBuzzSchema(1, 1, 2, "fizz", "buzz"));

        // Act
        evenementFizzBuzzListener.listen(record);

        // Verify
        FizzBuzzSchema value = record.value();
        Assertions.assertThat(fizzBuzzRepository.findByLimitAndInt1AndInt2AndStr1AndStr2(
                        value.getLimit(), 
                        value.getInt1(), 
                        value.getInt2(), 
                        value.getStr1(), 
                        value.getStr2()))
        .isNotNull()
        .satisfies(f -> f.getCount().equals(1))
        .usingRecursiveComparison()
        .ignoringFields("count", "id")
        .isEqualTo(value);
    }
    
    /**
     * RULE_INCREMENT_STORE_EVENT: Listen few records, then verify that count is incremented on database
     */
    @Test
    public void listen_records_and_check_that_count_increment() {

        // Prepare
        ConsumerRecord<String, FizzBuzzSchema> record =
                        new ConsumerRecord<>("topic", 0, 0, "key", createFizzBuzzSchema(1, 1, 2, "fizz", "buzz"));

        // Act
        evenementFizzBuzzListener.listen(record);
        evenementFizzBuzzListener.listen(record);
        evenementFizzBuzzListener.listen(record);

        // Verify
        FizzBuzzSchema value = record.value();
        Assertions.assertThat(fizzBuzzRepository.findByLimitAndInt1AndInt2AndStr1AndStr2(
                        value.getLimit(), 
                        value.getInt1(), 
                        value.getInt2(), 
                        value.getStr1(), 
                        value.getStr2()))
        .isNotNull()
        .satisfies(f -> f.getCount().equals(3))
        .usingRecursiveComparison()
        .ignoringFields("count", "id")
        .isEqualTo(value);
    }
    
    /**
     * RULE_INCREMENT_STORE_EVENT: Listen few kinds of records, then verify that count is incremented on database
     */
    @Test
    public void listen_few_kinds_of_records_and_check_that_count_increment() {

        // Prepare
        ConsumerRecord<String, FizzBuzzSchema> record1 =
                        new ConsumerRecord<>("topic", 0, 0, "key", createFizzBuzzSchema(1, 1, 2, "fizz", "buzz"));
        ConsumerRecord<String, FizzBuzzSchema> record2 =
                        new ConsumerRecord<>("topic", 0, 0, "key", createFizzBuzzSchema(1, 1, 2, "fozz", "bizz"));

        // Act
        evenementFizzBuzzListener.listen(record1);
        evenementFizzBuzzListener.listen(record1);
        evenementFizzBuzzListener.listen(record1);
        
        evenementFizzBuzzListener.listen(record2);
        evenementFizzBuzzListener.listen(record2);
        

        // Verify
        FizzBuzzSchema value1 = record1.value();
        Assertions.assertThat(fizzBuzzRepository.findByLimitAndInt1AndInt2AndStr1AndStr2(
                        value1.getLimit(), 
                        value1.getInt1(), 
                        value1.getInt2(), 
                        value1.getStr1(), 
                        value1.getStr2()))
        .isNotNull()
        .satisfies(f -> f.getCount().equals(3))
        .usingRecursiveComparison()
        .ignoringFields("count", "id")
        .isEqualTo(value1);
        
        FizzBuzzSchema value2 = record2.value();
        Assertions.assertThat(fizzBuzzRepository.findByLimitAndInt1AndInt2AndStr1AndStr2(
                        value2.getLimit(), 
                        value2.getInt1(), 
                        value2.getInt2(), 
                        value2.getStr1(), 
                        value2.getStr2()))
        .isNotNull()
        .satisfies(f -> f.getCount().equals(2))
        .usingRecursiveComparison()
        .ignoringFields("count", "id")
        .isEqualTo(value2);
    }
    
    private FizzBuzzSchema createFizzBuzzSchema(Integer limit, Integer int1, Integer int2, String str1, String str2) {
        final FizzBuzzSchema fizzBuzzSchema = new FizzBuzzSchema();
        fizzBuzzSchema.setLimit(limit);
        fizzBuzzSchema.setInt1(int1);
        fizzBuzzSchema.setInt2(int2);
        fizzBuzzSchema.setStr1(str1);
        fizzBuzzSchema.setStr2(str2);
        return fizzBuzzSchema;
    }

}
