package com.leboncoin.metrics.spy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Function;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery.FetchableFluentQuery;
import com.leboncoin.metrics.provider.entity.FizzBuzzMetricEntity;
import com.leboncoin.metrics.provider.repository.FizzBuzzRepository;

/**
 * This spy mock a database
 * 
 * It allow junit tests to not depends of the chosen queue system
 * 
 * Many method are not used, they need to be implement if they are used is tests
 *
 */
public class FizzBuzzRepositorySpy implements FizzBuzzRepository {
    
    /** Persistence entites */
    List<FizzBuzzMetricEntity> entities = new ArrayList<>();

    /**
     * Save of update a {@link FizzBuzzMetricEntity} on persistence entities.
     *
     * @param entity a {@link FizzBuzzMetricEntity}
     * @return created/updated entity
     */
    @Override
    public <S extends FizzBuzzMetricEntity> S save(S entity) {
        
        if(entity.getId() == 0) {
            entity.setId(new Random().nextLong());
            entities.add(entity);
        } else {
            entities.stream()
            .filter(o -> o.getId() == entity.getId())
            .forEach(o -> o = entity);
        }
        
        return entity;
    }
    

    /**
     * Find a {@link FizzBuzzMetricEntity} from persistence entities.
     *
     * @param limit, int1, int2, str1, str2: research criteries
     * @return {@link FizzBuzzMetricEntity} found
     */
    @Override
    public FizzBuzzMetricEntity findByLimitAndInt1AndInt2AndStr1AndStr2(Integer limit, Integer int1,
                    Integer int2, String str1, String str2) {

        Optional<FizzBuzzMetricEntity> element = entities.stream()
            .filter(e -> limit.equals(e.getLimit())
                            && int1.equals(e.getInt1())
                            && int2.equals(e.getInt2())
                            && str1.equals(e.getStr1())
                            && str2.equals(e.getStr2())).findAny();
        
        if(element.isPresent()) {
            return element.get();
        }
        
        return null;
    }

    /**
     * Find a {@link FizzBuzzMetricEntity} from persistence entities with maximum count
     *
     * @return {@link FizzBuzzMetricEntity} found
     */
    @Override
    public FizzBuzzMetricEntity findTopByOrderByCountDesc() {
        Optional<FizzBuzzMetricEntity> element = entities
                        .stream()
                        .max(Comparator.comparing(FizzBuzzMetricEntity::getCount));
        
        if(element.isPresent()) {
            return element.get();
        }
        
        return null;
    }
    
    @Override
    public List<FizzBuzzMetricEntity> findAll() { return null; }

    @Override
    public List<FizzBuzzMetricEntity> findAll(Sort sort) { return null; }

    @Override
    public List<FizzBuzzMetricEntity> findAllById(Iterable<Long> ids) { return null; }

    @Override
    public <S extends FizzBuzzMetricEntity> List<S> saveAll(Iterable<S> entities) { return null; }

    @Override
    public void flush() { }

    @Override
    public <S extends FizzBuzzMetricEntity> S saveAndFlush(S entity) { return null; }

    @Override
    public <S extends FizzBuzzMetricEntity> List<S> saveAllAndFlush(Iterable<S> entities) { return null; }

    @Override
    public void deleteAllInBatch(Iterable<FizzBuzzMetricEntity> entities) { }

    @Override
    public void deleteAllByIdInBatch(Iterable<Long> ids) { }

    @Override
    public void deleteAllInBatch() {}

    @Override
    public FizzBuzzMetricEntity getOne(Long id) { return null; }

    @Override
    public FizzBuzzMetricEntity getById(Long id) { return null; }

    @Override
    public <S extends FizzBuzzMetricEntity> List<S> findAll(Example<S> example) { return null; }

    @Override
    public <S extends FizzBuzzMetricEntity> List<S> findAll(Example<S> example, Sort sort) { return null; }

    @Override
    public Page<FizzBuzzMetricEntity> findAll(Pageable pageable) { return null; }

    @Override
    public Optional<FizzBuzzMetricEntity> findById(Long id) { return null; }

    @Override
    public boolean existsById(Long id) { return false; }

    @Override
    public long count() { return 0; }

    @Override
    public void deleteById(Long id) {  }

    @Override
    public void delete(FizzBuzzMetricEntity entity) { }

    @Override
    public void deleteAllById(Iterable<? extends Long> ids) { }

    @Override
    public void deleteAll(Iterable<? extends FizzBuzzMetricEntity> entities) { }

    @Override
    public void deleteAll() { }

    @Override
    public <S extends FizzBuzzMetricEntity> Optional<S> findOne(Example<S> example) { return null; }

    @Override
    public <S extends FizzBuzzMetricEntity> Page<S> findAll(Example<S> example, Pageable pageable) { return null; }

    @Override
    public <S extends FizzBuzzMetricEntity> long count(Example<S> example) { return 0; }

    @Override
    public <S extends FizzBuzzMetricEntity> boolean exists(Example<S> example) { return false; }

    @Override
    public <S extends FizzBuzzMetricEntity, R> R findBy(Example<S> example, Function<FetchableFluentQuery<S>, R> queryFunction) { return null; }


}
