# Application configuration variables

This documentation refers to all dynamic configurations of **Simple fizz-buzz REST server metrics**.

| Variable  | Description  | Default value |
|---|---|---|
| KAKFA_BROKER | Kafka broker hostname  | localhost:9093  |
| DATABASE_HOSTNAME | Database hostname  | localhost:5432  |
| DATABASE_SCHEMA | Database schema  | fizz  |
| DATABASE_USERNAME | Database username  | fizz  |
| DATABASE_PASSWORD | Database password  | buzz  |
| SCHEMA_REGISTRY | Kafka schema registry adress  | "http://localhost:8081" |
| KAFKA_FIZZ_BUZZ_TOPIC | Kafka output topic name | fizzbuzztopic |
| PUBLIC_KEY | Public key to decode JWT tokens| Check on application.yml|