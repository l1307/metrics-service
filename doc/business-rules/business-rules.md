# Business Rules

This documentation refers to all business Rules of the **Simple fizz-buzz REST server metrics**.

## Business rules

### RULE_INITIAL_STORE_EVENT
When this service consumes an event message, if no similar message is stored in the database, then store this message with a number of occurrences fixed at 1.

### RULE_INCREMENT_STORE_EVENT
When this service consumes an event message, if a similar message is stored in the database, then increment the number of occurrences of this message.

### RULE_API_METRICS
This service exposes an API which returns the message with the highest number of occurrences.

### RULE_API_METRICS_NOT_FOUND
This service exposes an API which returns a 404 error if there is nothing in the database.